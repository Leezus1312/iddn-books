package main

/* You need to compile this to make it work */
/* render <yaml> <template> */

import (
  "fmt"
  "gopkg.in/yaml.v2"
  "io/ioutil"
  "text/template"
  "bytes"
  "os"
  "regexp"
  "github.com/dvsekhvalnov/jose2go/base64url"
  "reflect"
  "strings"
  "github.com/yosssi/gohtml"
)

const PREFIX = "~"
const SEEK_PREFIX = "~~PCEtL" // ~~ + base64url encoded <!--

var re_SeekPrefix = regexp.MustCompile(`^`+SEEK_PREFIX)

type Subsection struct {
  Title string
  Body string
}

type Section struct {
  Title string
  Body string
  Subsections []Subsection
}

type Chapter struct {
  Title string
  Body string
  Sections []Section
}

type Book struct {
  Include_folder string
  Page_title string
  Page_color string
  Book_name string
  Last_updated string
  Introduction string
  Chapters []Chapter
}

func check(e error) {
  if e != nil {
      panic(e)
  }
}

var re_html = regexp.MustCompile(`\.html$`)

func main() {
  var buf bytes.Buffer

  if len(os.Args) != 3 {
    panic(fmt.Errorf("Wrong amount of parameters. render <Book> <Template>\n"))
  }

  /* read in book in yaml format */
  d, err := ioutil.ReadFile(os.Args[1]); check(err)

  /* parse yaml  */
  mybook := Book{}
  err = yaml.Unmarshal(d, &mybook); check(err)

  /* We need to preprocess d before Unmarshalling */
  if mybook.Include_folder != "" {

    /* setup yaml template data structure */
    includes := make(map[string]string)
    var prebuf bytes.Buffer

    /* there was an include folder listed, we need to include those things */
    dir, err := ioutil.ReadDir(mybook.Include_folder); check(err)
    for _, dirEntry := range dir {

      /* is it an html file? */
      isHtml, err := regexp.MatchString(`\.html$`,dirEntry.Name()); check(err)

      /* make sure it's not a directory now */
      if (dirEntry.IsDir()==false&&isHtml) {

        /* simplify the name to just the filename sans extension for key */
        include_key := re_html.ReplaceAllString(dirEntry.Name(),"")

        /* read data in */
        data, err := ioutil.ReadFile(mybook.Include_folder+"/"+dirEntry.Name()); check(err)

        /* add prefix and collaspe back to a single multi-line string */
        includes[include_key] = PREFIX + base64url.Encode(data)
      }
    }
    pre, err := template.ParseFiles(os.Args[1]); check(err)
    err = pre.Execute(&prebuf, includes); check(err)
    //fmt.Println(prebuf.String())
    err = yaml.Unmarshal(prebuf.Bytes(), &mybook); check(err)
  }

/*
  this is walking mybook and its children looking for special fields that start with the prefix.  WHen found base64url decode them and update the field in the book.  This all done dynamically using interfaces incase something changes in the future.
*/
  for chi, chaps := range mybook.Chapters {
    for sei, sects := range chaps.Sections {
      for sui, _ := range sects.Subsections {
        values := reflect.ValueOf(&mybook.Chapters[chi].Sections[sei].Subsections[sui]).Elem()
        // values := reflect.ValueOf(&subsects).Elem()
        //typeOfS := values.Type()
        for i := 0; i< values.NumField(); i++ {
          if (values.Field(i).Kind() == reflect.String && re_SeekPrefix.MatchString(values.Field(i).String())) {
            ans, err := base64url.Decode(strings.TrimLeft(values.Field(i).String(),"~"))
            if err == nil { 
              values.Field(i).SetString(string(ans))
            }
            fmt.Println("Bonzai!", values.Field(i).CanSet(), values.Field(i).String())
          }
        }
      }

      values := reflect.ValueOf(&mybook.Chapters[chi].Sections[sei]).Elem()
      for i := 0; i< values.NumField(); i++ {
        if (values.Field(i).Kind() == reflect.String && re_SeekPrefix.MatchString(values.Field(i).String())) {
          ans, err := base64url.Decode(strings.TrimLeft(values.Field(i).String(),"~"))
          if err == nil { 
            values.Field(i).SetString(string(ans))
          }
          fmt.Println("Bonzai!", values.Field(i).CanSet(), values.Field(i).String())
        }
      }
    }

    values := reflect.ValueOf(&mybook.Chapters[chi]).Elem()
    for i := 0; i< values.NumField(); i++ {
      if (values.Field(i).Kind() == reflect.String && re_SeekPrefix.MatchString(values.Field(i).String())) {
        ans, err := base64url.Decode(strings.TrimLeft(values.Field(i).String(),"~"))
        if err == nil { 
          values.Field(i).SetString(string(ans))
        }
        fmt.Println("Bonzai!", values.Field(i).CanSet())
      }
    }
  }

  values := reflect.ValueOf(&mybook).Elem()
  for i := 0; i< values.NumField(); i++ {
    if (values.Field(i).Kind() == reflect.String && re_SeekPrefix.MatchString(values.Field(i).String())) {
      ans, err := base64url.Decode(strings.TrimLeft(values.Field(i).String(),"~"))
      if err == nil { 
        values.Field(i).SetString(string(ans))
      }
      fmt.Println("Bonzai!", values.Field(i).CanSet())
    }
	}

  /* read in template in html format */
  tmpl, err := template.ParseFiles(os.Args[2]); check(err)

  /* pair the two together */
  err = tmpl.Execute(&buf, mybook); check(err)

  /* output to stdout */
  fmt.Println(gohtml.Format(buf.String()))
}
